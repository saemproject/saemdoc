.. _utilisation:
.. index:: guide-utilisation

============
Guide d'utilisation
============

Le Système d'archivage électronique mutualisé est découpé en trois modules qui interagissent entre eux :

.. index:: Modules applicatifs

Les modules du SAEM
----------------------

- gestion des données de référence ou référentiel : ce module permet de gérer les données relatives aux acteurs de l'archivage, aux vocabulaires contrôlés et aux profils de versement. Il est géré par les archivistes.
- gestion des processus d'archivage ou ged sas : ce module permet de gérer les différents processus d'archivage (versement, consultation, restitution, élimination) dans un espace documentaire dédié. Il est utilisé par les services versants et les archivistes.
- gestion de l'archivage et de la conservation ou as@lae : ce module permet de conserver les archives numériques de façon pérenne. Il est utilisé par les archivistes.


.. note::

   Ces modules sont construits à partir d'applications open source. La gestion des processus est outillée par une version 5 d'Alfresco community, la gestion de l'archivage par as@lae en version 1.6 et la gestion des référentiels par une application développée sur la base du framework cubicweb_.

.. warning::

    Ces modules peuvent fonctionner de manière indépendante pour une implémentation dans d'autres systèmes. Leur utilisation combinée dans le SAEM permet de couvrir l'ensemble des exigences fonctionnelles de l'archivage électronique et de simplifier les tâches de gestion récurrentes.

L’intégration du module Référentiel au cœur de l’architecture du projet SAEM permet de centraliser l’ensemble des données de référence dans une seule et même brique applicative.
Dans un objectif d'interopérabilité, le protocole OAI-PMH a été retenu pour réaliser les échanges de données par web services.
Ce protocole international étant un standard, il offre un moyen d'échanger et de moissonner des métadonnées d’archivage entre plusieurs institutions.
La GED SAS et Asalae récupèrent les données de référence suivantes depuis le référentiel :

•	Profils SEDA
•	Acteurs de l'archivage
•	Vocabulaires contrôlés & concepts

**Vision cible du projet SAEM girondin**

.. image:: ../img/visionCible.png
   :height: 386px
   :width: 601px
   :scale: 80%
   :alt: la cible
   :align: center

Après un déploiement des applications, l'utilisation de l'ensemble des modules du projet SAEM implique :

•	d'enregistrer les données de référence de l'archivage dans le module gestion des données de référence
•	de synchroniser les modules gestion des processus d'archivage et gestion de l'archivage avec le module gestion des données de référence.

.. _cubicweb: http://cubicweb.org/project/cubicweb-saem_ref
.. _documentation: https://wiki.alfresco.com/wiki/Scheduled_Actions#Cron_Explained

.. index:: gestion des données

Module gestion des données de référence
-----------------------------------------

Fonctionnalités
------------------------




Le module permet de produire, importer, gérer et rélier entre elles les données de référence produites par les archivistes. Ces données sont de plusieurs types :

* les acteurs de l'archivage : autorités administratives (collectivités) se composant d'unités administratives (services versants, producteurs, ou service d'archives), ainsi que les agents, contacts référents de ces entités.
* les notices d'autorité : description des producteurs (unités administratives ou producteurs privés) conforme à la norme ISAAR-CPF et suivant le schéma XML EAC.
* les vocabulaires contrôlés : listes d'autorité ou thésaurus (liste hiérarchisée). Il peut s'agir de vocabulaires externes officiels (ex : thésaurus et listes d'autorité du SIAF) ou internes (produits par les collectivités).
* les profils d'archivage : ensemble des règles définissant les contraintes liées à un versement d'archives, quelle que soit la version du SEDA.
* les unités d'archives : unités de description quel que soient leur niveau, réutilisables au sein d'un ou de plusieurs profil(s).

Tous les évènements affectant les entités profils et notices d'autorité (création, modification, publication, suppression le cas échéant), sont documentés dans un onglet cycle de vie.

Autres fonctionnalités :

* Import et export de notices d'autorité au format XML-EAC.
* Import de vocabulaires contrôlés publiés par des institutions de référence comme le SIAF ou la BNF au format SKOS ou de vocabulaires au format CSV.
* Exposition des données suivant le protocole OAI-PMH, en particulier pour leur synchronisation avec les modules de gestion des processus d'archivage et de conservation.
* Attribution d'identifiants ark aux entités créées (notices d'autorités, unités administratives, vocabulaires, concepts, profils), mais aussi aux archives (dossiers ou fichiers) dans le module de gestion des processus d'archivage (ark correspondant à l'identifiant de l'archive donné par le service versant) et dans le module de gestion de l'archivage et de la conservation (ark correspondant à la cote des archives).

**Focus sur le système ark** (Archival Ressource Key):

Un identifiant ark est l'identifiant perenne d'une ressource ou d'un objet disponible sur le web, quel que soit sa nature et indépendamment de modifications du site sur lequel elle/il apparait. C'est l'adresse de la ressource sur le net. Ce système est géré par la california digital library.

L'ark se compose de plusieurs parties :

* un préfixe correspondant à l'autorité nommante.
* une partie name composée d'un préfixe rf fixant le contexte d'attribution des identifiants.
* une longueur fixe de 10 caractères, un qualitatif de granularité.

Le référentiel est le numéroteur centralisé du système d'archivage électronique.

Administration du module
----------------------------------------


L'onglet **Administration** réservé aux administrateurs permet de :

* créer des autorités nommantes
* créer et gérer les utilisateurs et les groupes
* importer des vocabulaires au format SKOS présents sur le web
* visualiser le modèle de donnée de l'applicatif (complet, par type d'entité ou type de relations)

Créer une autorité nommante
============================

Au sein du référentiel, les autorités nommantes sont des instititutions habilitées à attribuer des ark aux différentes entités.

* Dans l'onglet Administration de la page d'accueil, aller dans autorité nommante ark

.. image:: ../img/Doc-SAEM-REF-OngletAdministration.png
   :scale: 80%
   :alt: la cible
   :align: center

* Cliquer sur le + pour en ajouter une.
* Renseigner le nom de l'autorité nommante et son identifiant fourni par la californian digital library.

.. image:: ../img/creation_autorite_naan.png
   :scale: 80%
   :alt: la cible
   :align: center

Créer et gérer les utilisateurs et les groupes
======================================

* Dans l'onglet Administration de la page d'accueil, aller dans utilisateurs et groupes

.. image:: ../img/Doc-SAEM-REF-OngletAdministration.png

* Cliquer sur nouvel utilisateur

.. image:: ../img/creer_nouvel_utilisateur.png

* Renseigner les éléments nécessaires
* Définir le groupe auquel le nouve utilisateur appartient (sélectionner user ou manager)

.. image:: ../img/creer_nouvel_utilisateur_2.png

* Cliquer sur la flèche pour passer l'information dans la fenêtre de droite
* Valider


Importer un vocabulaire skos présent sur le web
==================================================

Aller sur la dashlet ou l'onglet vocabulaire et cliquer sur l'icône importer.

.. image:: ../img/Doc-SAEM-Import_Voca_Skos_1.png
   :scale: 50%
   :align: center

Après avoir déclaré le titre, la description et rattaché le vocabulaire à une autorité nommante, vous pouvez soit créer manuellement les concepts soit importer un fichier contenant la description des concepts.

Si vous utilisez la syntaxe lcsv, la procédure d'import est décrite dans ce fichier_
.. _fichier: https://framagit.org/saemproject/saemdoc/raw/master/source/guide-utilisateur/import_lcsv_skos_complet.pdf?inline=false

 Exemple d'affichage d'un thésaurus :

.. image:: ../img/Doc-SAEM-Thesaurus.png
   :scale: 50%
   :align: center

A ce stade les vocabulaires créés ou importés sont en mode brouillon. Pour devenir accessibles aux autres modules, il est nécessaire de les publier. Lorsqu'ils le sont, il est toujours possible de rajouter des termes ou de les modifier mais plus de les supprimer.

Visualiser le modèle de donnée de l'applicatif
==================================================

Le modèle de données est visualisable sous forme d'un schéma dans le menu administration.

Données de référence
---------------------------------

Le module de gestion des données de référence contient plusieurs types de données reliées entre elles par une ontologie.

Les acteurs de l'archivage
============================

Les autorités administratives et leurs entités associées (unités administratives et agents) sont des acteurs du système d'archivage électronique
au sens du SEDA. Les unités administratives notamment se voient attibuer un rôle archivistique comme service versant, service producteur, service d'archives ou service de contrôle.

Lorsque les modules du SAEM sont synchronisés, les unités administratives et leurs contacts référents sont automatiquement déclarés comme intervenants et utilisateurs dans les modules gestion des processus d'archivage
et gestion de l'archivage et de la conservation.

Ces entités qui pré existent dans chaque collectivité en dehors de l'archivage électronique, sont déjà partiellement décrites dans des outils de gestion des ressources humaines (organigrammes, annuaires LDAP ou autres applications du sytème d'information). Ces outils internes pourront à terme créer automatiquement des unités administratives et des agents.

Au sein du référentiel, une **autorité administrative** est une collectivité (commune, département ou un établissement public) "mère",
composée de plusieurs **unités administratives** "filles" (directions ou services) et de x **agents** de type personne (employés).
L'autorité administrative n'a pas de rôle archivistique propre ; les rôles sont définis pour les unités.

Il est obligatoire d'associer un service d'archives à une autorité administrative lorsqu'on la crée.
Par héritage, toutes les unités administratives de cette autorité auront le même service d'archives qui est logiquement compétent pour toute la collectivité.

- Créer une autorité administrative ville de Bordeaux.

.. image:: ../img/Doc-SAEM-Crea_Autorite_Admin_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

.. image:: ../img/Doc-SAEM-Crea_Autorite_Admin_2.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

On dispose maintenant d'une autorité administrative, au sein de laquelle on peut créer des unités administratives "filles".

.. image:: ../img/Doc-SAEM-Crea_Unite_Admin_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

- Créer le service d'archives compétent pour toute la ville.

.. image:: ../img/Doc-SAEM-Crea_Unite_Admin_2.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

- Créer un contact pour le service d'archives. Cet agent sera le contact référent du service d'archives.

.. image:: ../img/Doc-SAEM-Crea_Agent_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

- L'agent créé apparaît dans une liste déroulante des contacts référents possibles du service d'archives, ce qui permet de le sélectionner.

.. image:: ../img/Doc-SAEM-Asso_Agent_UA.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

- Associer le service d'archives compétent à l'autorité administrative.

.. image:: ../img/Doc-SAEM-Asso_ServiceArchives_AutoriteAdministrative_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

- Créer les autres unités administratives "filles", pour les services versants. Pour chacun d'entre eux, créer un agent et l'associer au service versant en tant que contact référent.

.. image:: ../img/Doc-SAEM-Crea_Unite_Admin_3.png
   :scale: 50%
   :alt: capture d'écran de la page de paramétrage de l'unité administrative
   :align: center

Les acteurs de l'archivage sont maintenant créés.

Notices d'autorité
====================

Les notices d'autorités sont des fiches de description des producteurs d'archives, conformément à la norme ISAAR-CPF.
Le référentiel implémente le schéma EAC-CPF et permet ainsi d'importer, de créer et d'exporter des notices d'autorités dans un format
intéropérable avec d'autres applications.

Dans le référentiel, chaque notice d'autorité présente des informations sur un producteur qui sont classées dans les quatre permiers onglets (informations générales, description, propriétés, relations). Ces onglets correspondent aux zones de la norme ISAAR-CPF.
Un cinquième onglet "cycle de vie" correspond à l'enregistrement des évenements effectués sur la notice.

------------------------------
Créer une notice d'autorité
------------------------------

Exemple de la notice d'autorité de la direction de la petite enfance de la ville de Bordeaux.

On crée une notice d'autorité depuis l'onglet de la page d'accueil, en cliquant sur l'icône +.

.. image:: ../img/Doc-SAEM-Crea_NoticeAutorite_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

On peut également créer ou relier une notice d'autorité directement à partir de l'unité administrative à laquelle elle se rapporte.

.. image:: ../img/Doc-SAEM-Crea_NoticeAutorite_DepuisUA_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center


.. image:: ../img/Doc-SAEM-Crea_NoticeAutorite_DepuisUA_2.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center



Présentation des différents onglets des notices d'autorité :

- onglet **informations générales** :

.. image:: ../img/Doc-SAEM-Exemp_NoticeAutorite_1.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center

- onglet **description** :

.. image:: ../img/Doc-SAEM-Exemp_NoticeAutorite_2.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center

- onglet **propriétés** :

.. image:: ../img/Doc-SAEM-Exemp_NoticeAutorite_3.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center

- onglet **relations** :

.. image:: ../img/Doc-SAEM-Exemp_NoticeAutorite_4.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center

Exemple de relation chronologique : au sein du Département de la Gironde, le service de l'Administration Générale des Assemblées et du Courrier (SAGAC) existe jusqu'au 31 décembre 2007. Puis, un nouveau service de l'Administration Générale et des Assemblées (SAGA) est créé, qui prend sa suite. On doit donc créer 2 notices d'autorité : l'une pour le SAGAC l'autre pour le SAGA en spécifiant leur dates d'existence. On complète ensuite la notice du SAGAC en indiquant qu'il est le précédesseur du SAGA. La relation en tant que successeur se fait automatiquement sur la notice du SAGA. Dans ce cas, les dates de la relation chronologique sont les mêmes que les dates d'existence des services.

.. image:: ../img/relations_chrono.png
   :scale: 50%
   :alt: relations chronologiques
   :align: center

-------------------------------
Importer une notice d'autorité
-------------------------------

* Cliquer sur l'icône importer

.. image:: ../img/import_notice_1.png
   :scale: 50%
   :alt: importer 1
   :align: center

* Choisir un fichier en allant le sélectionner

.. image:: ../img/import_notice_2.png
   :scale: 50%
   :alt: importer 2
   :align: center

*  Cliquer sur importer

.. image:: ../img/import_notice_3.png
   :scale: 50%
   :alt: importer 3
   :align: center

Le fichier est importé. Des avertissements sur d'éventuelles erreurs dans le format XML sont mentionnées mais elles n'empêchent pas l'import.

Pour les utilisateurs du logiciel Arkhéïa de la société Anaphore, un lien est en cours de réalisation entre le module acteur et le module référentiel. Ceci leur permettra de gérer les noms des fournisseurs (dans Arkhéïa) de façon harmonisée avec le référentiel et d'y associer des notices producteurs créées dans le référentiel conformément à ISAAR CPF (norme non implantée dans Arkhéïa).

Vocabulaires
===============

Les vocabulaires sont des listes de termes organisées. Elles peuvent prendre la forme de listes simples (listes d'autorité) ou hiérarchiques (thésaurus).

Il est possible de créer des vocabulaires mais également d'en importer.

---------------------------------------------------------
Créer un vocabulaire directement dans le référentiel
---------------------------------------------------------

Aller sur la dashlet ou l'onglet vocabulaire et cliquer sur l'icône +. Vous devez donner à ce vocabulaire :

- un titre : nom du vocabulaire
- une description : qualification du contenu du vocabulaire
- autorité nommante ARK : rattachement à une autorité d'archivage

.. image:: ../img/Doc-SAEM-Crea_Voca_1.png
   :scale: 50%
   :align: center

Une fois créé, le vocabulaire se voit attribué un identifiant unique. Vous pouvez ensuite créer des concepts. Pour ajouter un concept vous pouvez saisir :

- une définition :
- un exemple d'utilisation
- un ou plusieurs libellés
- sélectionner un type de libellé (préféré ou alternatif) et un code langue

.. image:: ../img/Doc-SAEM-Crea_Concept_1.png
   :scale: 50%
   :align: center

Une fois le concept créé vous pouvez ajouter un nouveau concept au même niveau ou un sous-concept de celui que vous venez de créér.

.. image:: ../img/Doc-SAEM-Affichage_Concepts.png
   :scale: 50%
   :align: center

---------------------------------------------------------------------------------------------------------
Créer un vocabulaire en téléchargeant un fichier existant (tableur numérique ou fichier linked csv)
---------------------------------------------------------------------------------------------------------

2 syntaxes sont possibles :

- format de fichier simple : un tableur numérique (excel ou calc par exemple) ne contenant qu'une colonne
- format de fichier linked csv : un tableur avec une syntaxe particulière permettant l'import de thésaurus hiérarchiques.

Une section de la documentation détaille plus précisément la syntaxe attendue. Voir `mon fichier <https://framagit.org/saemproject/saemdoc/raw/master/source/import_lcsv_skos_complet.pdf>`_


Profils SEDA
===============

Pour préparer des versements automatisés ou réguliers d'archives électroniques, l'étude du flux permet de définir un plan de classement type, ainsi que le contenu attendu et des règles de gestion (DUA, sort final ou communicabilité). L'ensemble des règles définies constitue un profil SEDA, lui-même composé de plusieurs unités d'archives et d'objets données associés. Dans un premier temps, il convient de créer des unités d'archives qui seront réutilisables dans n'importe quel profil.

-------------------------------
Créer une unité d'archives
-------------------------------

On peut créer une Unité d'Archives (UA) pour n'importe quel niveau de description (dossier, sous-dossier, pièce, collection, etc.). Le niveau de description constitue l'un des champs obligatoires.

* Cliquer sur l'icône + pour créer l’UA

Onglet informations générales / description
#############################################

.. image:: ../img/unité_archives_1.png
   :scale: 50%
   :alt: unité d'archives 1
   :align: center

* Le premier pavé "aide à la saisie" correspond au nom de cette UA dans le référentiel.
* Renseigner l’autorité nommante ark.
* Renseigner les champs nécessaires. Ceux marqués d'un carré rouge sont obligatoires.
* Le champ titre apparaîtra dans la ged sas. Pour le renseigner, 2 cas de figures se présentent :

1. Le titre est fixé à l'avance, il peut donc être renseigné dans le champ valeur et ne sera pas modifiable par le service versant lors de la préparation d’un versement.

2. Le titre sera renseigné par le service versant lors de la préparation d’un versement, il faut donc laisser le champ valeur vide et actionner l'aide à la saisie (en cliquant sur +) afin de préciser les éléments attendus.

.. image:: ../img/unité_archives_3.png
   :scale: 50%
   :alt: unité d'archives 3 titre
   :align: center

* Ouvrir les champs que l'on souhaite faire renseigner lors de la préparation du versement (ex : date de début, date de fin)
* Il est recommandé de renseigner "la langue de contenu" dès la conception de l'UA. Cliquer sur le + puis sur « Relier à concept ». Dans la zone de recherche, saisir « french » (ou une autre valeur si besoin), cocher ce concept et valider.

.. image:: ../img/unité_archives_2.png
   :scale: 50%
   :alt: unité d'archives 2 langue
   :align: center

Onglet gestion
###############

Cet onglet permet de renseigner les DUA, sort final et délais de communicabilité. Faire glisser la souris et cliquer sur le + vert.

Onglet unités d’archives
########################

Cet onglet permet d’ajouter une UA déjà existante ou d’en créer une nouvelle au sein de l’UA en cours de création.

Onglet objets-données
########################

Cet onglet permet de joindre un ou plusieurs fichier(s) à archiver au moment du versement.

* Ajouter un objet binaire
* Dans le champ "aide à la saisie", donner un intitulé pour décrire le fichier attendu
* Un algorythme d'empreinte doit être renseigné. Par défaut, c'est SHA 256 car c'est celui qui est utilisé par As@lae.  L'algorythme d'empreinte par défaut utilisé dans la ged sas est SHA 256 (mais il n'est pas mis par défaut dans le référentiel). Si un mode d'encodage est spécifié, dans la ged sas, au moment de compléter le formulaire, un message indique (en plus du type de format attendu), le type d'encodage attendu. Si le fichier sélectionné par le service versant n'est pas dans l'encodage attendu, ça ne bloque pas le versement, et il n'a pas de message particulier, mais au moment de la soumission du versement au service d'archives, un nouveau message d'alerte précise à l'attention de l'archiviste : encodage non conforme à ce qui est attendu. Toutefois, on peut quand même accepter le versement.
* Cliquer sur « attachement »
* Valider

.. image:: ../img/objet_binaire.png
   :scale: 50%
   :alt: objet binaire
   :align: center

* Ouvrir l'objet binaire créé
* Renseigner des éléments de description si besoin
* Dans l'onglet format, renseigner la catégorie de fichier attendu. 6 catégories sont définies dans un vocabulaire "format" (image, audio, compression, document, données structurées, vidéo). On peut sélectionner une ou plusieurs catégorie(s) et au sein de ces catgéories, une ou plusieurs extension(s) attendue(s).

.. image:: ../img/formats.png
   :scale: 50%
   :alt: format
   :align: center

Onglet indexation
########################

Cet onglet permet d’ajouter un ou plusieurs descripteur(s) contrôlé(s) ou libre(s).
Renseigner la cardinalité du ou des descripteur(s), le type de vocabulaire attendu. En fonction du type choisi, les vocabulaires associés sont disponibles. On peut saisir un concept en dur ou laisser le champ valeur vide pour qu’il soit complété lors du versement.

Historique de conservation
###############################

Cet onglet permet d'ajouter des éléments sur le cycle de vie du fichier avant son versement et d'activer un horodatage.

-------------------------------
Créer un profil d'archives
-------------------------------

* Cliquer sur l'icône + pour créer le profil.

Onglet informations générales
##############################

* Renseigner le nom du profil dans le champ "titre".
* Renseigner l'autorité nommante
* Valider

.. image:: ../img/profil_1.png
   :scale: 50%
   :alt: profil
   :align: center

Onglet description
####################

* Ne pas renseigner le premier champ "aide à la saisie".
* Le contenu du **champ commentaire sert de titre du versement pendant toute la procédure dans la GED SAS, jusqu'à sa validation dans As@lae**. 2 cas de figure se présentent :

1. Le titre est fixé à l'avance, il peut donc être renseigné dans le champ valeur et ne sera pas modifiable par le service versant lors de la préparation d’un versement.

2. Le titre sera renseigné par le service versant lors de la préparation d’un versement, il faut donc laisser le champ valeur vide et actionner l'aide à la saisie (en cliquant sur +) afin de préciser les éléments attendus.

* Valider

* Renseigner l'accord de service en reprenant le même intitulé que celui saisi dans As@lae.
* Valider

.. image:: ../img/profil_2.png
   :scale: 50%
   :alt: profil 2
   :align: center

Onglet gestion
####################

Cet onglet permet de renseigner les DUA, sort final et délais de communicabilité. Faire glisser la souris et cliquer sur le + vert.

Les valeurs choisies seront propagées dans la totalité du profil si elles n'ont pas été renseignées dans les UA.

.. image:: ../img/profil_3.png
   :scale: 50%
   :alt: profil 3
   :align: center

Onglet unité d'archives
##########################

**Recommandations**

* Il est conseillé de faire une UA de premier niveau qui englobera les autres et constituera l'enveloppe du versement final dans As@ale. Si cela n'est pas fait, chaque UA de premier niveau deviendra une archive dans As@lae.
* Il est conseillé d'utiliser des UA précédemment créées pour constituer le profil. On pourra les modifier autant que de besoin.

.. image:: ../img/profil_4.png
   :scale: 50%
   :alt: profil 4
   :align: center

.. image:: ../img/profil_5.png
   :scale: 50%
   :alt: profil 5
   :align: center

* Après validation, les unités d'archives apparaîssent dans la zone "arbre du profil SEDA" dont elles constituent l'arborescence.

 .. image:: ../img/profil_6.png
   :scale: 50%
   :alt: profil 6
   :align: center

Finalisation et association d'un profil à un service versant
##############################################################

Une fois le profil créé, on doit passer de l'état "brouillon" à  "publié" dans la zone "actions - profil SEDA" pour rattacher le profil à un ou plusieurs service(s) versant(s).

 .. image:: ../img/profil_7.png
   :scale: 50%
   :alt: profil 7
   :align: center

Pour associer un profil à un service versant :

* Se positionner sur l'unité administrative à laquelle on souhaite associer le profil
* Dans l'onglet "vocabulaire / profils", cliquer sur ajouter profil SEDA.
* Sélectionner le ou les profil(s) choisi(s). (Seuls les profils publiés apparaissent dans cette liste).

 .. image:: ../img/profil_8.png
   :scale: 50%
   :alt: profil 8
   :align: center

Lorsqu'un profil est devenu caduque, il est possible de le dupliquer afin d'en créer une nouvelle version actualisée. Pour cela
* Se positionner sur le profil, dans la zone "actions - profil SEDA", cliquer sur "nouvelle version".
* Une copie du profil est réalisée, sur laquelle on peut faire toutes les modifications nécessaires.
* Une fois le nouveau profil validé, l'ancien est automatiquement déprécié.

.. index:: gestion des processus d'archivage

Module gestion des processus d'archivage
---------------------------------------
Ce module est accessible par les services versants et les archivistes. C'est un sas d'échange pour la réalisation des processus d'archivage.

Fonctionnalités
~~~~~~~~~~~~~~~
Ce module est le lieu d'échange entre les services versants et les services archives. Il permet de réaliser les processus liés aux archives (versements, éliminations, communications, restitutions) et d'organiser les versements des services.

La plupart des données qui permettent au module de fonctionner sont issues du référentiel. Celui-ci permet en effet de créer des sites collaboratifs, des catégories et des dossiers dits "profilables" dans les espaces documentaires des services versants automatiquement.
Une action visible depuis chaque site collaboratif Share et depuis le tableau de bord des administrateurs Alfresco permet de lancer manuellement la synchronisation sans attendre l'exécution de la tâche planifiée. Cette synchronisation est assurée par des webservices  respectant le protocole OAI-PMH.

Lancée depuis le tableau de bord des administrateurs Alfresco, la synchronisation est dite globale.
Lancée depuis un site collaboratif, la synchronisation est dite spécifique.

Paramétrage initial
~~~~~~~~~~~~~~~~~~~~

Créer des utilisateurs GED SAS
====================================

La création des utilisateurs de la GED SAS quel que soit leur rôle (même s'ils ont déjà été créés dans le référentiel pour les contacts référents) est obligatoire dans la GED SAS en attendant une synchronisation Alfresco-Active Directory (liste des comptes informatiques)

-	Se connecter en tant qu'administrateur

.. image:: ../img/creationUtilisateurEtape1.png
   :scale: 100%
   :alt: écran membre site
   :align: center

-	Accéder à l’interface des utilisateurs
-	Choisir le menu « Nouvel utilisateur »

.. image:: ../img/creationUtilisateurEtape2.png
   :height: 350px
   :width: 1300px
   :scale: 80%
   :alt: écran membre site
   :align: center

-	Saisir à minima les valeurs obligatoires distinguées par un astérisque en veillant à bien respecter l'orthographe du nom saisi dans le référentiel (car il n'y a pas de synchronisation automatique pour les protagonistes)
-	Valider en fonction des actions par le bouton « Créer un utilisateur » ou « Créer, puis en créer un autre »


.. image:: ../img/creationUtilisateurEtape3.png
   :height: 823px
   :width: 700px
   :scale: 80%
   :alt: écran membre site
   :align: center

Associer un utilisateur à un site versant
====================================
L'utilisateur qui a été créé dans Alfresco doit être associé au site versant

-	si c'est l'utilisateur Contact référent du site versant, il aura automatiquement le rôle Gestionnaire après la synchronisation

-   si c'est le contact référent du site d'archives associé au site versant, il aura automatiquement le rôle Contributeur après la synchronisation

-	si c'est une autre personne agissant au sein du site versant et qui n'est donc pas saisi dans le référentiel, il faut l'invite avec un rôle Contributeur afin de pouvoir modifier tous les contenus

Pour cela, dans le site versant, aller dans Membres du Site (1) puis Inviter des personnes (2)

.. image:: ../img/InvitationDansAlfresco.png
   :height: 300px
   :width: 870px
   :scale: 80%
   :alt: Menu Invitation
   :align: center

Ensuite, rechercher la personne (1), puis Ajouter (2), et lui donner le rôle Contributeur (3)

.. image:: ../img/InvitationDansAlfresco2.png
   :height: 197px
   :width: 870px
   :scale: 80%
   :alt: Fonctions recherche et rôle dans invitation
   :align: center

 Enfin, lancer l'invitation (1):

.. image:: ../img/InvitationDansAlfresco3.png
   :height: 453px
   :width: 634px
   :scale: 80%
   :alt: Envoi de l'invitation
   :align: center

La personne recoit un mail d'invitation. Elle peut ainsi accéder au site versant.


Synchroniser avec le référentiel
==============================

Une première synchronisation doit être réalisée en tant qu'administrateur de la GED SAS. Elle assure la synchronisation du vocabulaire contrôlé "catégories de fichier" utilisé dans le cadre du formulaire de préparation des versements pour indiquer aux services le type de format attendu.

La synchronisation des données du Référentiel avec la GED SAS est assurée par une tâche planifiée à paramétrer sur le serveur de la GED SAS. Sa périodicité est définie via la propriété « synchro.cronExpression » mentionné dans le fichier de propriétés « saem.properties » du serveur.

Lancer une "synchro Référentiel" pour exécuter manuellement le processus de synchronisation avec le module référentiel.

.. image:: ../img/synchroSite_GEDSAS.png
   :height: 197px
   :width: 870px
   :scale: 80%
   :alt: écran synchronisation
   :align: center

Ce processus ne peut être exécuté de manière simultanée. Si une demande de synchronisation est effectuée alors qu’une autre est déjà en cours de traitement, un message prévient l’utilisateur de cette information :

.. image:: ../img/synchroEnCours.png
   :height: 108px
   :width: 496px
   :scale: 80%
   :alt: écran synchronisation
   :align: center

La GED SAS importe sous forme de sites collaboratifs Share les unités administratives (services ou directions) publiées dans le référentiel. Elle les distingue en fonction du rôle archivistique qui leur a été attribué par le référentiel :

* Unités administratives ayant le rôle « service versant » (qui ont un thème de couleur verte),
* Unités administratives ayant le rôle « service archive » (qui peuvent aussi avoir un rôle de service versant, et qui ont une thème de couleur violette).

Un site versant possède les caractéristiques suivantes :

* Un tableau de bord normalisé intégrant les dashlets spécifiques « Mes actions » et « Mes traitements »:

.. image:: ../img/dashboardSiteVersant.png
   :height: 620px
   :width: 1042px
   :scale: 80%
   :alt: écran tableau de bord
   :align: center

* Un gestionnaire correspondant au « contact référent » qui lui est associé dans le module Référentiel.

Un site archives possède les caractéristiques suivantes :

* Un tableau de bord normalisé intégrant les dashlets spécifiques « Mes actions » et « Mes traitements »
* Une dashlet supplémentaire « Mes sites versants » qui permet de lister l’ensemble des sites versants associés au site archive.

.. image:: ../img/dashletMesSitesVersants.png
   :height: 298px
   :width: 572px
   :scale: 80%
   :alt: écran dashlet Mes services versants
   :align: center


Préparer le site service archives
=================================

* Se connecter dans la GED SAS avec le compte utilisateur créé pour le gestionnaire du site archives (contact référent associé dans le référentiel).
* Dans le site archives, indiquer l'accord de versement :

    1. Cliquer sur l'engrenage en haut à droite

    2. Modifier les détails du site

    3. Compléter l'identifiant de l'accord de versement paramétré dans As@lae
    
* Le mécanisme de synchronisation reprend le gestionnaire du site d'archive et crée ses droits en tant que contributeur sur les sites versants afin qu'il puisse intervenir sur les versements. Mais ce mécanisme n'est opérant que pour le gestionnaire du site archives tel que défini dans le référentiel. Il est nécessaire que le gestionnaire du site service versant invite les autres archivistes créés dans la GED SAS avec le rôle contributeur.  

**Inviter un nouvel archiviste sur un site d'archive**

* Cliquer sur le bouton "Inviter des utilisateurs" dans le dashboard principal du site (bouton silhouette en haut à droite)

 .. image:: ../img/invitation_utilisateur_1.png
   :height: 200px
   :width: 870px
   :scale: 80%
   :alt: image choix utilisateur
   :align: center

* Choisir un utilisateur interne (déja créé dans la GED SAS) ou inviter un utilisateur externe :

 .. image:: ../img/invitation_utilisateur_2.png
   :height: 350px
   :width: 900px
   :scale: 100%
   :alt: image choix utilisateur
   :align: center


* Lui attribuer le rôle de gestionnaire du site.

 .. image:: ../img/invitation_utilisateur_3.png
   :height: 350px
   :width: 900px
   :scale: 100%
   :alt: image choix du role
   :align: center

* valider

 .. image:: ../img/invitation_utilisateur_4.png
   :height: 350px
   :width: 900px
   :scale: 100%
   :alt: image confirmation invitation
   :align: center

* Le nouvel utilisateur va dans la dashlet "mes tâches" et clique sur "Invitation à rejoindre le site".


Préparer le site service versant
=================================

* Se connecter dans la GED SAS avec le compte utilisateur créé pour le gestionnaire du site versant (contact référent associé dans le référentiel).

* Dès la création du site, inviter les archivistes qui ont été créés dans la GED SAS (autres que celui positionné comme référent dans le référentiel), avec le rôle contributeur.

Pour chaque service versant créé dans Alfresco, une synchronisation est réalisée avec les profils utilisables créés dans le référentiel. Pour chaque profil associé à un service donné dans le référentiel, un dossier de type « profilable » est créé à la racine de l’espace documentaire correspondant.

Suite à la synchronisation des données, deux fichiers XML sont créés dans le répertoire « Dictionnaires de données » Alfresco : « synchroVocabulaires.xml » et « synchroProfils.xml » :

.. image:: ../img/fichiersRapportSynchro.png
   :height: 263px
   :width: 601px
   :scale: 80%
   :alt: écran synchronisation
   :align: center

Ces fichiers spécifient deux éléments

* La date de dernière synchronisation pour chaque élément concerné (profils et vocabulaires contrôlés). Cette information permet de lancer les synchronisations ultérieures en mode différentiel permettant de ne récupérer que les éléments ayant été modifiées ou ajoutés depuis la dernière synchronisation.
* Les éléments n’ayant pu être synchronisés durant le processus de synchronisation globale. Ces éléments sont alors tracés dans le fichier XML et seront traités durant la prochaine synchronisation.


Renseigner l'accord de versement dans la GED SAS
==================================================================

Cette fonctionnalité est uniquement accessible aux gestionnaires de sites. L’accord de versement doit être spécifié pour tous les sites archives et versants.

1. Cliquer sur « Modifier les détails du site », en haut à droite de la barre de navigation (icône engrenage)

2. Renseigner le champ accord de versement en recopiant l’identifiant de l’accord de versement créé dans as@lae

3. Ne pas modifier la visibilité (cochée "privée" par défault)

.. image:: ../img/fenetresaisieAccordversement.png
   :height: 381px
   :width: 384px
   :scale: 80%
   :alt: paramétrage accord versement ged sas
   :align: center

4. Cliquer sur OK


Synchronisation manuelle spécifique
=================================

Elle permet d’exécuter manuellement le processus de synchronisation avec le module Référentiel pour le site versant concerné ou pour le site d'archive concerné et ses sites versants dépendants.
Ce type de synchronisation est soumise aux mêmes règles d'execution que la synchronisation globale.

Elle met à jour uniquement le fichier « synchroProfils.xml ».

**Synchronisation des profils SEDA**

.. image:: ../img/profilSeda.png
   :height: 113px
   :width: 331px
   :scale: 80%
   :alt: écran synchronisation
   :align: center

Les profils SEDA existant à l'état publié dans le module Référentiel sont synchronisés. Ils sont exportés au format XSD et stockés dans le répertoire « Dictionnaire de Données/Profils SEDA » de l’Entrepôt Alfresco.

Le fichier « synchroProfils.xml » créé dans le répertoire « Dictionnaire de données » permettant de spécifier la date de dernière synchronisation des profils, est alors mis à jour.

Dans le cas où certains profils n’ont pu être synchronisés, le fichier XML intègre également la liste de ces profils en échec afin de les prendre en charge durant la synchronisation suivante.
Durant les synchronisations différentielles, si un profil existant est mis à jour dans le Référentiel, une nouvelle version de ce dernier est créée dans la GED SAS, ce qui permet de conserver un historique pour chaque profil.

**Synchronisation des vocabulaires contrôlés**

.. image:: ../img/vocControles.png
   :height: 295px
   :width: 415px
   :scale: 80%
   :alt: un agent
   :align: center

Seuls les vocabulaires contrôlés à l’état publié sont synchronisés avec la GED SAS. Les vocabulaires contrôlés et les concepts définis correspondent dans la GED SAS aux catégories spécifiées dans la catégorie racine « Vocabulaires » :

.. image:: ../img/ecranCategorieVoc.png
   :height: 519px
   :width: 537px
   :scale: 80%
   :alt: catégories et vocabulaires
   :align: center

Durant la phase de synchronisation spécifique, si un concept ou un vocabulaire est modifié, la catégorie correspondante est supprimée de la GED SAS puis recréée selon la nouvelle définition du référentiel.

Le fichier « synchroVocabulaires.xml » créé dans le « Dictionnaire de données » est mis à jour lors de chaque synchronisation. Il spécifie notamment la date de dernière synchronisation, la liste des vocabulaires n’ayant pu être synchronisés pour les traiter durant la synchronisation suivante.


Présentation des éléments de la GED SAS
===================================

Page d'accueil / tableau de bord
--------------------------------------------------

La page d'accueil de la GED SAS se présente sous forme d'un tableau de bord à partir duquel un utilisateur accède aux sites pour lesquels il a les droits.

Les dashlets des sites
---------------------------------

---------------------------
Dashlet "Mes traitements"
---------------------------

Cette dashlet est disponible dans les tableaux de bord des sites versant ou archives.

.. image:: ../img/dashletMesTraitements.png
   :height: 233px
   :width: 825px
   :scale: 100%
   :alt: dashlet mes traitements
   :align: center

Elle présente la liste des versements et traitements à effectuer par les utilisateurs. Dans un site archives, l'intitulé "auteur" est remplacé par "service versant". Les éléments affichés peuvent être triés en cliquant sur l'entête de chaque colonne.

Les actions possibles sont les suivantes :

* crayon bleu : permet de modifier un versement
* plus vert : permet de lancer le worflow de versement par le service versant
* plus rouge : permet de modifier un versement et de relancer le worflow après un refus
* V vert : permet de valider un versement par le service archives
* cloche orange : permet d'indiquer au service archive que le versement est à valider dans as@lae
* flèche bleue : permet de confirmer un versement par un service versant
* oeil bleu : permet de visualiser des documents demandés en communication
* croix orange : permet de proposer une élimination par un service versant ou un service archives
* croix verte : permet de valider l'élimination par le service versant ou le service archives
* flèche violette : permet de lancer le worflow de restitution par le service versant

-----------------------
Dashlet "Mes actions"
-----------------------

Elle permet de lancer les processus métier liés aux archives : versement, élimination, restitution.

---------------------------
Dashlet "Archives éliminables"
---------------------------

Elle indique les archives éliminables dont la DUA est échue. Elle permet donc de lancer le worflow d'élimination.

---------------------------
Dashlet "Activités du site"
---------------------------

Elle récapitule les derniers évènements.

---------------------------
Dashlet "Profil du site"
---------------------------

Elle présente les informations concernant le service.

Sur un site versant, on trouve les informations suivantes : le nom du service, le site archive référent, le nom du coordinateur, les profils associés pour effectuer des versements, l'état de la visibilité.

.. image:: ../img/profilsiteversant.png
   :height: 350px
   :width: 900px
   :scale: 80%
   :alt: profil site versant
   :align: center

Sur un site archives, on trouve les informations suivantes : le nom du site, l'accord de versement utilisé, le statut de l'archiviste connecté, l'état de la visibilité.

.. image:: ../img/profilsitearchive.png
   :height: 350px
   :width: 900px
   :scale: 80%
   :alt: profil site archive
   :align: center

L'icone à droite du nom du site permet de renvoyer aux informations présentes dans le référentiel.

---------------------
Dashlet "Mes tâches"
---------------------

Elle permet de voir les invitations reçues et répertorie les tâches à effectuer. Il est possible de trier ces dernières selon leur statut. Plusieurs actions sont possibles depuis cette dashlet telles qu'éditer une tâche, démarrer un workflow, etc.

---------------------
Dashlet "Contenu du site"
---------------------

Elle permet de suivre les modifications apportées, d'en visualiser le détail en cliquant dessus et d'effectuer une modification le cas échéant.


Gérer les processus d'archivage
===========================

Workflow de versement
-------------------------------------

La préparation d’un versement est la première étape du cycle de vie documentaire. Cette action permet de constituer un pré-versement correspondant à un profil d’archivage. Ce pré-versement est ensuite soumis par le service versant à un workflow de versement vers le module d'archivage et de conservation.

-------------------------------------------------------------------------------------
Préparer un versement avec un formulaire
-------------------------------------------------------------------------------------

Initier le pré-versement
##############################

1. Cliquer sur « Préparer un versement ».
Cette icône est disponible à deux emplacements :

* Dans la rubrique « Mes actions » du tableau de bord du site « Service Versant »

.. image:: ../img/icone_versement_1.png
   :height: 132px
   :width: 525px
   :scale: 80%
   :alt: icone versement
   :align: center

* Sur l’espace documentaire du site « Service Versant »

.. image:: ../img/icone_versement_2.png
   :height: 132px
   :width: 800px
   :scale: 80%
   :alt: icone versement
   :align: center

2. Une boite de dialogue apparait permettant de sélectionner le profil d’archive souhaité dans une liste déroulante. Cliquer sur OK pour générer un formulaire de versement.

.. image:: ../img/choisirProfil.png
   :height: 132px
   :width: 525px
   :scale: 80%
   :alt: choisir profil versement ged sas
   :align: center

Compléter le formulaire de versement
#######################################

Le formulaire de versement est automatiquement généré en fonction du profil SEDA sélectionné.

Les champs obligatoires (désignés par un astérisque) doivent être complétés

* Pour ajouter un élément facultatif ou répétable, cliquer sur le bouton « plus » bleu (1)
* Pour supprimer un élément facultatif, cliquer sur la croix rouge (2)
* Pour faire apparaitre les commentaires d’aide à la saisie, cliquer sur le point d’interrogation à côté du champ concerné (3)
* Les chiffres rouges apparaissant dans l'arborescence de gauche indiquent le nombre de champs obligatoires à renseigner (4)
* Les icônes en haut à droite du formulaire permettent de passer du mode complet au mode simplifié. Le mode simplifié ne fait apparaitre que les champs qui ne sont pas pré-saisis dans le profil (5)
* Une icône dans le fil d'ariane permet de visualiser le profil dans le module référentiel (6)

Les valeurs renseignées lors de la création du profil SEDA dans le référentiel sont les valeurs définitives et sont donc non modifiables dans la GEDSAS.

.. image:: ../img/formulaireVersementGedSas.png
   :height: 480px
   :width: 1031px
   :scale: 80%
   :alt: formulaire versement ged sas
   :align: center

Joindre les fichiers à verser
####################################

La zone documents permet de joindre les pièces à verser en accompagnant les utilisateurs sur les formats attendus. En effet, la gedsas récupère les types mimes et les formats autorisés spécifiés dans le profil. Lors de la création ou la modification d'un versement, un message renseigne donc l'utilisateur sur le type d'extension attendu.

.. image:: ../img/choixExtension.png
   :height: 200px
   :width: 900px
   :scale: 100%
   :alt: choix extension
   :align: center


Après le téléchargement du fichier, le module effectue automatiquement deux actions :

1. Contrôle des types de fichier

Si l'extension du fichier choisi ne correspond pas à celle attendue, un message d'avertissement apparaît

.. image:: ../img/mauvaisChoixExtension.png
   :height: 200px
   :width: 650px
   :scale: 100%
   :alt: mauvais choix extension
   :align: center


Si l'utilisateur appuie sur "OK", le fichier se télécharge et un nouveau message apparaît, sans bloquer le processus de versement en cours.

.. image:: ../img/mauvaisChoixExtension2.png
   :height: 200px
   :width: 900px
   :scale: 80%
   :alt: mauvais choix extension 2
   :align: center

2. Contrôle des fichiers déjà dans le versement

Pour éviter des doublons dans un versement, un controle est effectué sur un éventuel doublon. Dans ce cas, ce message d'erreur apparaît : "un fichier portant le même nom a déjà été sélectionné".


4. Calcul des dates

Pour chaque unité d’archives, les dates extrêmes sont automatiquement définies par la date de dernière modification des documents téléchargés.

La date de début est la date de création ou de dernière modification du document le plus ancien.

La date de fin est la date de création ou de dernière modification du document le plus récent.

La date de fin renseigne automatiquement la date de départ de calcul de la DUA et de la communicabilité.

Enregistrer le versement
##############################

Enregistrer en mode brouillon
+++++++++++++++++++++++++++

Cette fonctionnalité permet à l'utilisateur d'enregistrer un versement en cours de préparation (icône de dossier jaune). Il pourra reprendre ultérieurement cette préparation en choississant de l'action "Modifier" sur le tableau de bord "Mes traitements".

.. image:: ../img/modificationBrouillon.png
   :height: 300px
   :width: 850px
   :scale: 100%
   :alt: modification brouillon
   :align: center

On peut aussi modifier un versement préalablement enregistré en mode brouillon en y accédant par l'espace documentaire.

Lorsque l'on revient sur le versement ayant été enregistré en brouillon, les messages d'avertissement relatifs au format des fichiers sont conservés.

Enregistrer un versement finalisé
++++++++++++++++++++++++++++++++++

Quand l’utilisateur considère le versement comme complet il peut enregistrer le bordereau via le bouton « Enregistrer le bordereau de versement ». L'icône du dossier passe alors en orange.

Le workflow de versement entre le service versant et le service archives peut alors démarrer.

Nom des versements :

* Si le champs commentaire du nom du profil est renseigné avec une valeur définie, il est non modifiable dans le formulaire de la GED SAS et envoyé sans modification vers as@lae. La valeur est associés à un timestamp pour générer un nom de dossier unique dans l’espace documentaire Alfresco. Cette valeur est désigne le dossier du versement dans la dashlet « Mes traitements ».
* Si le champs commentaire du nom du profil est obligatoire avec une valeur libre (cardinalité 1), le champs est modifiable et obligatoire dans le formulaire de la GED et la valeur saisie est envoyée vers as@lae. La valeur saisie est aussi associée à un timestamp pour générer un nom de dossier unique dans l’espace documentaire Alfresco.
* Si le champs commentaire du nom du profil est demandé avec une valeur libre non obligatoire (cardinalité 0-1), le champs est modifiable dans le formulaire de la GED SAS. Si la valeur est saisie, le comportement précédant s'applique. Si la valeur n’est pas saisie, le comportement qui suit s'applique.
* Si le champs commentaire du nom du profil n’est pas demandé, le champs n’existe pas dans le formulaire. Un nom système « Versement » associé à un timestamp est créé. Aucune balise sur le commentaire n'est transmise à as@lae.

Lors de la création ou la modification du versement, une erreur peut intervenir au moment de l'enregistrement.
Par exemple si le service ARK ne répond pas alors qu'il doit attribuer des identifiants à chaque éléments du versement (archive, documents..).
L'enregistremment ne sera pas effectué et un message d'alerte préviendra l'utilisateur :

.. image:: ../img/erreur_technique.png
   :height: 200px
   :width: 650px
   :scale: 80%
   :alt: erreur_technique_versement
   :align: center

Soumettre le versement au service Archives
#############################################

La soumission d’un versement s’effectue depuis un site versant. Par cette action, un service versant propose un versement au service archives.

Elle peut être lancée au niveau du dossier de versement, de la dashlet « mes traitements » ou de la dashlet « mes actions ».

A partir du dossier de versement :

.. image:: ../img/soumettreVersement.png
   :height: 123px
   :width: 829px
   :scale: 80%
   :alt: soumettre versement ged sas
   :align: center

A partir de la dashlet "mes traitements" :

  .. image:: ../img/VerserViaMestraitements.png
   :height: 250px
   :width: 900px
   :scale: 100%
   :alt: soumettre versement ged sas
   :align: center

A partir de la dashlet "mes actions"

  .. image:: ../img/Verserviamesactions.png
   :height: 250px
   :width: 900px
   :scale: 100%
   :alt: soumettre versement ged sas
   :align: center

Après avoir cliqué sur le bouton "soumettre aux archives", une boîte de dialogue apparaît permettant d'ajouter un commentaire si besoin.
Puis, cliquer sur "verser"

.. image:: ../img/Soumettreversementcommentaire.png
   :height: 350px
   :width: 829px
   :scale: 80%
   :alt: soumettre versement ged sas
   :align: center

Vérification des formats de fichier
########################################

Une vérification des fichiers basée sur leur extension est effectuée en fonction des formats attendus définis dans le profil. Si un fichier non conforme au format attendu est joint à un versement, une alerte de non conformité apparaît à destination du service archives.

.. image:: ../img/AlerteWF.png
   :height: 440px
   :width: 760px
   :scale: 100%
   :alt: extensions de fichiers non conformes aux extensions attendues
   :align: center

Les formats doivent correspondre au SEDA 0.2 afin que les versements soient conformes dans as@lae. Pour cela un ensemble de règles a été définie :

.. image:: ../img/formats_seda02.png
   :height: 60px
   :width: 900px
   :scale: 100%
   :alt: regles_formats_seda02
   :align: center

Accepter le versement (par le service archives)
################################################

Le(s) coordinateur(s) du site d’archive voi(en)t apparaître une nouvelle tâche dans la dashlet « Mes tâches » du site et au niveau de la dashlet "Mes traitements" :

S'il existe un seul gestionnaire du site archives, la tâche lui est assignée automatiquement. S'il en existe plusieurs, les propositions de versements se placent dans la dashlet "Mes tâches".

.. image:: ../img/Assignation.png
   :width: 855px
   :height: 364px
   :scale: 80%
   :alt: assignation
   :align: center

Chaque gestionnaire peut traiter le versement directement ou s'assigner la tâche préalablement au traitement.

Pour procéder à la validation, cliquer sur le bouton « Accepter » :

.. image:: ../img/validationVersement.png
   :width: 849px
   :height: 488px
   :scale: 80%
   :alt: validation versement ged sas
   :align: center

Une fois le versement soumis, l’icône du dossier prend la couleur bleue.

.. image:: ../img/versementSoumis.png
   :height: 131px
   :width: 826px
   :scale: 80%
   :alt: versement ged sas soumis
   :align: center

Valider le versement (par le service versant)
##############################################

Suite à l'acception du versement par le service archives, le gestionnaire du site versant voit s’afficher une tâche « Confirmation du versement ».

Dans la dashlet "Mes traitements" du service versant, cliquer sur le bouton « Valider le versement ».

.. image:: ../img/validerVersement.png
   :width: 855px
   :height: 364px
   :scale: 80%
   :alt: confirmation validation versement ged sas
   :align: center

Suite à cette opération, la taille des documents versés est vérifiée.

Si la taille des documents est inférieure à 300 Mo, le versement est transmis directement au module de gestion de l'archivage et de la conservation.

Si la taille des documents est supérieure à 300 Mo, le versement passe dans un statut « En attente fichier volumineux » (icône marron). Un processus spécifique de gestion des fichiers volumineux se lance. A son achèvement, l'icône repasse en bleu.

La validation du versement équivaut à la signature du bordereau par le service versant. Elle envoie le versement dans le module de gestion de l'archivage et de la conservation, où il sera traité par le service archives voir `Accepter un transfert dans Asalae <http://saem.readthedocs.io/fr/master/guide-utilisateur/asalae.html#accepter-un-transfert-dans-asalae>`_

Corriger un versement
######################

Lorsqu’un versement est rejeté par le service archive à partir de la GED SAS, une tâche de correction du versement est assignée à son auteur. Pour corriger le versement :

* Aller dans l'espace documentaire
* Ouvrir le dossier profilable correspondant
* Se positionner sur le versement sans l'ouvrir
* Cliquer sur "modifier l'archive".

.. image:: ../img/correction_versement.PNG
   :width: 986px
   :height: 432px
   :scale: 80%
   :alt: correction versement
   :align: center

L’auteur du versement peut alors le modifier et le soumettre à nouveau au service archives.

-----------------------------
Préparer un versement manuel
-----------------------------

La GED SAS permet de créer des versements de façon manuelle (sans passer par le formulaire). Cette possiblité sera principalement utilisée par les archivistes.
Le modèle de données Alfresco a été personnalisé afin de l’adapter aux exigences fonctionnelles du projet.

La création manuelle d’un versement peut se faire dans un site versant et dans un versement en cours (répertoire créé sous le répertoire « profilable ») via les actions suivantes :

Ajouter un fichier dans un dossier de l'espace documentaire
##################################################################

Choisir le dossier dans lequel on veut ajouter un fichier, cliquer sur « Importer dans l’entrepôt ».

Lors de l’import d’un fichier (via bouton « Importer dans l’entrepôt » ou drag & drop) dans un dossier « profilable », un formulaire de saisi est imposé à l’utilisateur pour renseigner ces métadonnées :

.. image:: ../img/aspectArchivesDocGedSas.png
   :height: 639px
   :width: 657px
   :scale: 80%
   :alt: aspect archives document ged sas
   :align: center

Actuellement les propriétés spécifiées sur les documents sont les suivants :

* Nom
* Titre
* Description
* Typologie de document
* Date de validation
* Format
* Contrôle
* Identification Unique (Ark)

La valorisation de l’identifiant unique (ARK) positionné sur le document est automatique et fournie par le Référentiel via la consommation d’un webservice REST soumis à authentification.

.. image:: ../img/identifiantArkDoc.png
   :width: 419px
   :height: 316px
   :scale: 80%
   :alt: identifiant ark document ged sas
   :align: center

Ajouter un répertoire dans un versement
############################################

Choisir le dossier dans lequel on veut ajouter un répertoire, cliquer sur « Créer », puis "nouveau dossier". Le formulaire de création suivant est affiché :

.. image:: ../img/creationRepertoireDossierGedSas.png
   :width: 538px
   :height: 852px
   :scale: 80%
   :alt: identifiant ark document ged sas
   :align: center

Les propriétés suivantes sont alors saisissables :

* Nom

* Titre

* Date de début

* Date de fin

* Description

* Niveau de description

* Indexation (positionnement des concepts)

* Règle à appliquer (pour la DUA/Sort final)

* DUA

* Date de départ de calcul

* Règle à appliquer (pour l’accès/communicabilité)

* Date de départ de calcul

Les métadonnées positionnées sont alors visibles depuis la vue détaillée du répertoire :

.. image:: ../img/vueDetailleFolderGEDSas.png
   :width: 538px
   :height: 621px
   :scale: 80%
   :alt: propriétés dossiers document ged sas
   :align: center

Identiquement aux documents, un identifiant ARK est automatiquement récupéré depuis le Référentiel et positionné sur le répertoire.

-------------------------------------------------
Préparer un versement en important un fichier ZIP
-------------------------------------------------

La création manuelle d’un versement peut être également réalisée via l’import d’un fichier ZIP dans un dossier « profilable ». L’utilisateur prépare alors sur son poste l’arborescence documentaire associée au versement suivant le profil cible :

.. image:: ../img/cibleZipDossier.png
   :width: 198px
   :height: 106px
   :scale: 80%
   :alt: propriétés dossiers document ged sas
   :align: center

Pour chaque unité documentaire, les fichiers d’archives sont ajoutés.
L’utilisateur crée alors le zip à partir de cette arborescence :

.. image:: ../img/creationArboZip.png
   :width: 609px
   :height: 100px
   :scale: 80%
   :alt: création dossier zip pour ged sas
   :align: center

Le fichier zip est alors importé dans le dossier profilable du site versant associé :

.. image:: ../img/importerDossierZipGedSas.png
   :width: 515px
   :height: 267px
   :scale: 80%
   :alt: import dossier zip pour ged sas
   :align: center

Pour rappel, les dossiers « profilable » sont situés à la racine de l’espace documentaire de chaque site versant.
L’utilisateur procède alors à la décompression de l’archive via l’action « Décompresser » :

.. image:: ../img/decompressionZipGedSas.png
   :width: 747px
   :height: 303px
   :scale: 80%
   :alt: decompression dossier zip pour ged sas
   :align: center

Le système affiche la boîte de dialogue « Extraire vers… » :

.. image:: ../img/extraireGedSas.png
   :width: 813px
   :height: 259px
   :scale: 80%
   :alt: extraction dossier zip pour ged sas
   :align: center

Suite à la décompression :

* Un répertoire « Versement 1 » est créé. Ce dernier contient l’ensemble de l’archive ZIP importé.
* L’archive ZIP est supprimée de l’espace documentaire du site.
* Un identifiant ARK est attribué pour chaque élément (document/répertoire) du versement.

.. image:: ../img/creationDossierExtraction.png
   :width: 559px
   :height: 180px
   :scale: 80%
   :alt: creation dossier zip pour ged sas
   :align: center

Durant la décompression de l’archive ZIP, un contrôle de conformité est réalisée pour vérifier la conformité du versement décompressé avec le profil SEDA défini par le dossier.
Dans cet exemple, le profil SEDA attend l’arborescence documentaire suivante :

* Séance de l’assemblée
   * accuse
   * délibération
   * signature

Les règles actuelles mises en œuvre durant le contrôle de conformité sont les suivantes :

* Si un répertoire autre que ceux définis dans le profil SEDA est présent dans l’archive ZIP, il est décompressé puis supprimé (Par exemple : existence d’un répertoire « temp » sous « Séance de l’assemblée »).
* Si un répertoire de l’archive ZIP contient n documents et que le profil stipule une cardinalité de 1 pour ce répertoire, l’ensemble des documents sont décompressés et conservés car aucune information complémentaire ne permet à ce jour de définir le seul fichier à retenir dans le lot.

L’ensemble de ces règles pourront évoluer selon les besoins fonctionnels.
L’apport d’un fichier XML complémentaire au ZIP pourra être envisagé pour définir les métadonnées associées à chacun des éléments du versement.

Workflow de communication
============================

-------------------------------------------
Action de communication dans la GED
-------------------------------------------

Une archive versée non éliminée ou restituée peut être totalement ou partiellement communiquée :

1. Depuis la dashlet mes traitements si elle a été versée depuis moins de sept jours en cliquant sur l’icône "livre".

.. image:: ../img/communication_depuis_dashlet.png
   :width: 922px
   :height: 100px
   :scale: 100%
   :alt: workflow demande de communication archives
   :align: center

2. Depuis l’espace documentaire au niveau des actions proposées sur les archives versées.

Une fois la communication demandée, la demande est transmise directement à as@lae. Si elle est acceptée par l'archiviste, une icône (oeil bleu) apparaît dans la dashlet mes traitements, permettant de visualiser et de télécharger les documents pendant un semaine. (Cette durée est paramétrable).

-----------------------------------------------------------------------
Prise en compte de la demande de communication par le service archive
-----------------------------------------------------------------------

Lorsqu’une demande de communication est effectuée, les membres du service archive voient une nouvelle tâche dans leur liste de traitements.

.. image:: ../img/workflowComm.png
   :width: 496px
   :height: 142px
   :scale: 80%
   :alt: workflow communication archives
   :align: center

Cette tâche est une simple notification pour signaler une demande de communication sur un dossier et renseigner sur son état.

Workflow d’élimination
==========================

Une demande d’élimination peut être effectuée pour les archives versées dont la DUA est expirée.

.. _warning:
Pour qu’une demande d’élimination puisse être traitée dans as@lae, l’outil de conversion de document Cloudoo doit être correctement paramétré.

----------------------------------------------------------------------------
Demande d'élimination depuis un site versant ou un site archives
----------------------------------------------------------------------------

L’action d’élimination peut être lancée de trois manières différentes :

1. Depuis la dashlet "archives dont la DUA est expirée"
2. Depuis la dashlet des traitements, une icône X est disposée à côté des actions possibles
3. Depuis la dashlet « mes Actions », les dossiers éliminables sont affichés

Lorsque qu'une archive dont la DUA est expirée a été versée depuis le site archive (cas d’un site archive versant), l’action d’élimination peut être lancée depuis la dashlet « mes traitements », la dashlet « mes actions » ou l’espace documentaire.

Lorsque qu'une archive éliminable a été versé par un service versant, le service archives ne peut l'éliminer sans obtenir son approbation. Une icône X est disposée au niveau des actions possibles de la dashlet « mes traitements ». Elle permet de lancer le workflow d'élimination. D'abord vert pomme dans le site du service archives, la croix passe en orange dans le site du service versant, puis en cas d'acceptation, revient en rouge dans le site du service archives.

A NOTER : Un service versant sollicité par son service archives pour une élimination a la possibilité de modifier la DUA pour allonger la durée de conservation.

Le formulaire de lancement du workflow se présente ainsi.

.. image:: ../img/interfaceWFElimination.png
   :width: 852px
   :height: 322px
   :scale: 80%
   :alt: interface workflow élimination archives
   :align: center

**Validation du service versant**

Une fois la demande d’élimination effectuée, une nouvelle tâche est affectée aux utilisateurs du site versant.
Elle est aussi présentée au niveau de la dashlet « mes traitements » avec l’icône X.
Le service versant peut alors confirmer l’élimination ou demander un changement de DUA du versement.

**Changement des DUA**
Lorsque le service versant demande un changement des DUA, une nouvelle tâche de changement des DUA est affectée au service archive.
L’icône sablier est présentée dans la dashlet « mes traitements » au niveau des actions possibles sur le dossier. Le changement de DUA devra être effectué dans as@lae. Une fois la modification effectuée, le service archive pourra terminer la tâche en cliquant sur l’icône.

**Elimination**
Lorsque le service versant confirme l’élimination, une nouvelle tâche d’élimination est affectée au service archive.
L’icône X est présentée dans la dashlet « mes traitements » au niveau des actions possibles sur le dossier. Elle permet de terminer la tâche et d’envoyer la demande d’élimination à as@lae.

Workflow de restitution
==========================

-------------------------------------------
Action de restitution dans la GED
-------------------------------------------

Une archive versée peut être entièrement restituée. L’action de restitution peut être lancée de deux endroits différents :

1. Depuis la dashlet « mes actions »

.. image:: ../img/restitutionVersement.png
   :width: 521px
   :height: 315px
   :scale: 80%
   :alt: restitution versement ged sas
   :align: center

2. Depuis l’espace documentaire au niveau des actions proposées sur les archives versées.
Le filtre sur les documents permet de visualiser les archives versées.

.. image:: ../img/flitreArchivesVersees.png
   :width: 986px
   :height: 432px
   :scale: 80%
   :alt: filtre archives versées ged sas
   :align: center

Une fois la restitution demandée, la demande est transmise directement à as@lae et une nouvelle tâche est affectée aux membres du service archive.

Pour que les restitutions soient opérationnelles, la balise TransferringAgencyArchiveIdentifier doit être présente dans l'export RNG et XSD du profil.
Cette balise est systématiquement transmise par le référentiel et est masquée à l'affichage dans la GED SAS car elle est renseignée automatiquement à l'enregistrement du versement.

-------------------------------------------
Prise en compte de la demande de restitution par le service archive
-------------------------------------------

Lorsqu’une demande de restitution est effectuée, les membres du service archive voient une nouvelle tâche dans leur liste de traitements.

.. image:: ../img/workflowRestitution.png
   :width: 493px
   :height: 176px
   :scale: 80%
   :alt: workflow restitution archives
   :align: center

Cette tâche est une simple notification pour signaler une demande de restitution sur un dossier.

Recherche Avancée
======================

Le formulaire de recherche avancée est accessible uniquement aux utilisateurs appartenant au groupe des administrateurs. Il est accessible à partir de la barre de menus (en haut à droite).

.. image:: ../img/lienFormSearch.png
   :width: 749px
   :height: 96px
   :scale: 80%
   :alt: lien formulaire recherche
   :align: center

La recherche avancée propose un formulaire spécifique en fonction du type de données recherchées.

.. image:: ../img/typeContenuSearch.png
   :width: 880px
   :height: 306px
   :scale: 80%
   :alt: type contenu formulaire recherche
   :align: center

Il faut choisir le type de contenu « Dossiers d’archives » pour effectuer des recherches sur les archives. Le formulaire présente un champ de saisie ou de sélection pour les différentes informations pouvant caractérisée une archive.

**Enregistrement des recherches**

Un bouton "sauvegarder" se trouvant en haut et en bas du formulaire permet d’enregistrer les critères d’une recherche. La liste des recherches avancées peut être gérée depuis la dashlet « Mes recherches ». Un utilisateur devra l’ajouter pour la visualiser.

La croix permet de supprimer une recherche.
Pour lancer une recherche, il faut cliquer sur non nom.

**Présentation des résultats de recherche**

Les résultats de recherches sont proposés sous forme de liste avec une navigation à facettes permettant de trier facilement les résultats.

.. image:: ../img/facetteRecherche.png
   :width: 336px
   :height: 347px
   :scale: 100%
   :alt: facettes formulaire recherche
   :align: center

.. index:: module de gestion de l'archivage

Module - gestion de l'archivage et de la conservation
-----------------------------------------------------
Ce module fonctione avec le logiciel open source as@lae développé par la société Libriciel.

Le code source est disponible sur la `forge <https://adullact.net/projects/asalae/>`_ de l'éditeur.

.. image:: ../img/schema_asalae.png
   :width: 673px
   :height: 274px
   :scale: 100%
   :alt: schéma fonctionnel asalae
   :align: center

Fonctionnalités
~~~~~~~~~~~~~~~~
Ce module permet de valider le versement des paquets d'informations soumis par le module de gestion des processus d'archivage et de les transformer en paquets d'information à archiver.

Une fois connecté, l'utilisateur habilité peut gérer le versement, l'élimination, la conservation, la communication et la restitution des archives électroniques sous sa responsabilité.

Paramétrage initial
~~~~~~~~~~~~~~~~~~~~

Créer les services de synchronisation du référentiel
================================================================

Se connecter en administrateur.

Aller dans Administration technique / Compteurs

Editer les compteurs 
   1. ArchivalAgencyArchiveIdentifier
   2. ArchivalAgencyObjectIdentifier
   3. ArchivalAgencyDocumentArchiveIdentifier
   4. ArchivalAgencyDocumentObjectIdentifier

Sélectionner le type "compteur externe" puis sélectionner le driver du compteur "Identifiant ARK SAEM CD33"

.. image:: ../img/compteurs.PNG
   :width: 1288px
   :height: 513px
   :scale: 90%
   :alt: compteurs
   :align: center

.. image:: ../img/identifier.PNG
   :width: 923px
   :height: 762px
   :scale: 80%
   :alt: identifier
   :align: center
   
   
Configuration du connecteur spécifique
================================================================

1. Il faut purger tout résidus d’une précédente tentative de configuration. Il ne doit rester que la collectivité mère.

- Dans Administration SEDA / Acteurs SEDA, supprimer les acteurs pré existants le cas échéant

- Dans Administration / Rôles utilisateurs, supprimer les rôles pré existants le cas échéant

- Dans Administration / Collectivités-organismes, supprimer les collectivités pré existantes. Si le bouton supprimer n’apparait pas à côté d’une collectivité, alors un élément dépendant existe encore dans le schéma.

2. Associer un identifiant à la collectivité mère (celle à laquelle est rattaché le service archives) : dans l'onglet "informations principales" de la collectivité, renseigner "identifiant unique" par COLREF. Le nom donné sera "Collectivité de référence" afin de ne pas confondre avec la collectivité "Bordeaux métropole" par exemple. 

.. image:: ../img/colref.PNG
   :width: 906px
   :height: 399px
   :scale: 100%
   :alt: colref
   :align: center


3. Dans Administration / Rôles utilisateurs, attribuer les rôles adéquats à la collectivité de référence. 



4. Dans Administration / Référentiels extérieurs, configurer les référentiels 

- Référentiel extérieur collectivités

.. image:: ../img/connecteur_collectivites.PNG
   :width: 782px
   :height: 558px
   :scale: 100%
   :alt: connecteur collectivités
   :align: center

- Référentiel extérieur services et utilisateurs

.. image:: ../img/connecteur_services.PNG
   :width: 944px
   :height: 523px
   :scale: 100%
   :alt: connecteur services
   :align: center

- Référentiel extérieur profils d'archives

.. image:: ../img/connecteur_profils.PNG
   :width: 810px
   :height: 491px
   :scale: 100%
   :alt: connecteur profils
   :align: center

- Référentiel extérieur vocabulaires contrôlés

.. image:: ../img/connecteur_vocabulaires.PNG
   :width: 797px
   :height: 474px
   :scale: 100%
   :alt: connecteur vocabulaires
   :align: center

- Référentiel extérieur notices d'autorité
 
 .. image:: ../img/connecteur_notices.PNG
   :width: 802px
   :height: 434px
   :scale: 100%
   :alt: connecteur notices
   :align: center

Une fois les référentiels extérieurs configurés, lancer la synchronisation. 


Synchroniser des éléments du référentiel
==========================================

La synchronisation des différents éléments peut être lancée individuellement en cliquant sur l’icône loupe présentée dans l’interface puis en exécutant une des deux actions suivantes proposées en bas de page de visualisation du référentiel :

.. image:: ../img/detailSynchroAsalae.png
   :width: 900px
   :height: 500px
   :scale: 100%
   :alt: paramétrage synchro asalae
   :align: center

« Lancer la mise à jour du référentiel » : cette action effectue une mise à jour différentielle en synchronisant uniquement les éléments modifiés depuis la dernière synchronisation.

Une tâche planifiée est également disponible depuis le menu Administration Technique > Tâches planifiées afin d’effectuer une sychronisation complète de l’ensemble des référentiels extérieurs actifs.

Un compte utilisateur sera créé par As@lae pour chaque contact référent d’une unité administrative ayant le rôle archivistique « archive » ou « producteur ». La convention de nommage des identifiants utilise la première lettre du prénom suivie du nom. Par exemple le contact référent « Pierre Daniel » aura l’identifiant de connexion « pdaniel ». Le mot de passe initial est identique à l’identifiant de connexion. Lors de sa première connexion Pierre Daniel pourra utiliser le mot de passe « pdaniel ».

.. image:: ../img/creationUserAsalae.png
   :width: 732px
   :height: 385px
   :scale: 80%
   :alt: creation user asalae
   :align: center

.. warning::
Une réinitialisation du mot de passe sera demandée à la première connexion.

Créer un utilisateur non présent dans le référentiel
===============================================================

Si un archiviste supplémentaire doit être ajouté :

1. Aller dans Administration / Utilisateurs / Ajouter un utilisateur
2. Choisir la collectivité-organisme de rattachement
3. Remplir les champs demandés dans les deux 1ers onglets (les onglets 3 et 4 sont remplis automatiquement) :
4. Valider

Créer un accord de versement
================================

Au moins un accord de versement par défault est obligatoire pour l’acceptation d’un versement. Il est également possible de créer des accords de versement spécifiques. S’il n’est pas précisé au moment du versement, c’est l’accord de versement par défaut qui sera pris en compte.

Se connecter avec un compte personnel archiviste.

1. Aller dans le menu Administration SEDA / Accords de versement
2. Cliquer sur “Ajouter un Accord de versement”
3. Remplir les champs obligatoires

   * l'identifiant correspond au champ de la balise "Archival agreement" du profil SEDA (n'utiliser ni espace ni accent). Cet identifiant est à renseigner dans la GED SAS.

   * sélectionner un service archive

   * sélectionner 0, 1 ou plusieurs service(s) versant(s) (CTRL+A). Il est conseillé de ne sélectionner aucun service et de faire un accord par défault en cochant la case prévue à au point 4.

   * Choisir le volume de stockage principal

4. Cocher « Autoriser tous les producteurs pour cet accord de versement (déclaration d'un contrat pour chaque producteur non nécessaire) »

.. image:: ../img/accordversement1.png
   :width: 900px
   :height: 550px
   :scale: 100%
   :alt: type composition étapes
   :align: center

.. image:: ../img/accordversement2.png
   :width: 702px
   :height: 200px
   :scale: 100%
   :alt: type composition étapes
   :align: center

5. Valider
6. Editer l’accord de versement créé
7. Dans l’onglet Contrôles, sous « Pièces jointes » cocher « Validation des pièces jointes : en cas de non validité, générer dune aletre au lieu d'une erreur, et considérer le transfert conforme ».
8. Valider

Créer les circuits de traitement
====================================

Il est nécessaire de créer des circuits de traitement pour toutes les opérations liées aux archives : versement, élimination, restitution, communication. Pour cela, se connecter avec un compte personnel archiviste.
Les circuits de traitement peuvent faire intervenir plusieurs rôles ou profils dans les étapes de contrôle ou de validation nécessaires à la finalisation des différents processus.

-------------------------------------------------------------------------------------
Circuit de traitement des transferts
-------------------------------------------------------------------------------------

1. Aller dans Administration / Circuits de traitement / Ajouter un circuit de traitement.
2. En "Type de circuit", sélectionner "traitement de transfert d'archives"
3. Renseigner le formulaire comme ci-dessous

.. image:: ../img/traitementArchives.png
   :width: 588px
   :height: 297px
   :scale: 80%
   :alt: paramétrage traitement archives
   :align: center

4. Valider
5. Retourner dans la liste des circuits de traitement et aller dans les étapes (drapeau vert) du circuit nouvellement créé.
6. Ajouter une étape "validation" et renseigner le formulaire comme ci-dessous

.. image:: ../img/circuitTraitement.png
   :width: 781px
   :height: 271px
   :scale: 80%
   :alt: circuit traitement archives
   :align: center

7. Valider
8. Cliquer sur l’icône groupe bleu pour composer l’étape
9. Cliquer sur le lien « Ajouter une composition»
10. Choisir « Service d’archives » au niveau du type de composition

.. image:: ../img/compositionArchives.png
   :width: 184px
   :height: 90px
   :scale: 80%
   :alt: type composition archives
   :align: center

11. Valider

-------------------------------------------------------------------------------------
Circuit de traitement des demandes de communication
-------------------------------------------------------------------------------------

1. Aller dans Administration / Circuits de traitement / Ajouter un circuit de traitement.
2. En "Type de circuit", sélectionner "traitement des demandes de communication"
3. Renseigner un nom
4. Valider
5. Retourner dans la liste des circuits de traitement et aller dans les étapes (drapeau vert) du circuit nouvellement créé.
6. Ajouter une étape
7. La nommer "validation" et lui attribuer le type simple
8. Valider
9. Cliquer sur l’icône groupe bleu pour composer l’étape
10. Cliquer sur le lien « Ajouter une composition»
11. Choisir « Service d’archives » au niveau du type de composition
12. Valider

-------------------------------------------------------------------------------------
Circuit de traitement des demandes d'élimination
-------------------------------------------------------------------------------------

1. Aller dans Administration / Circuits de traitement / Ajouter un circuit de traitement.
2. En "Type de circuit", sélectionner "traitement des demandes d'élimination"
3. Renseigner un nom
4. Valider
5. Retourner dans la liste des circuits de traitement et aller dans les étapes (drapeau vert) du circuit nouvellement créé.
6. Ajouter une étape
7. La nommer "validation" et lui attribuer le type simple
8. Valider
9. Cliquer sur l’icône groupe bleu pour composer l’étape
10. Cliquer sur le lien « Ajouter une composition»
11. Choisir « Service d’archives » au niveau du type de composition
12. Valider

------------------------------------------------------------
Circuit de traitement des demandes de restitution
------------------------------------------------------------

1. Aller dans Administration / Circuits de traitement / Ajouter un circuit de traitement.
2. En "Type de circuit", sélectionner "traitement des demandes de restitution"
3. Renseigner un nom
4. Valider
5. Retourner dans la liste des circuits de traitement et aller dans les étapes (drapeau vert) du circuit nouvellement créé.
6. Ajouter une étape
7. La nommer "validation" et lui attribuer le type simple
8. Valider
9. Cliquer sur l’icône groupe bleu pour composer l’étape
10. Cliquer sur le lien « Ajouter une composition»
11. Choisir « Service d’archives » au niveau du type de composition
12. Valider

Traitements à opérer
===================

Accepter un transfert dans As@lae
----------------------------------

Les dossiers transférés apparaissent dans « Mes Transferts à traiter » quelques secondes après avoir été transférés depuis la GED SAS.

1. Se placer dans le sous menu Transferts, Mes transferts à traiter
2. Cocher la case à gauche de la ligne du transfert concernée
3. Choisir « Accepter » dans la liste déroulante
4. Cliquer sur le bouton « Accepter »

.. image:: ../img/validerversementAsalae.png
   :width: 800px
   :height: 305px
   :scale: 100%
   :alt: confirmation validation versement as@lae
   :align: center

5. Répéter l’opération autant de fois que nécessaire

.. note::
Une tâche planifiée au niveau de la GED est dédiée à la récupération des messages SEDA émis par As@lae. Il faut attendre quelques minutes pour que le message d’acceptation remonte au niveau de la GED SAS. A ce moment-là le dossier de versement aura une couleur verte et le statut versé.

Accepter un transfert non conforme dans As@lae
===============================================

Les dossiers présents dans les transferts non conformes peuvent être traités.

1. Se placer dans le sous menu Transferts, Transferts non conformes
2. Cliquer sur le sigle orange à droite

.. image:: ../img/sigle_orange.png
   :width: 800px
   :height: 107px
   :scale: 100%
   :alt: sigle orange
   :align: center

3. Sur l'onglet Traitement, choisir le traitement à appliquer (insérer dans un circuit de traitement), puis choisir le circuit adapté et cliquer sur Insérer dans un circuit

.. image:: ../img/onglet_traitement.png
   :width: 402px
   :height: 152px
   :scale: 100%
   :alt: onglet traitement
   :align: center

4. Le dossier sera transféré dans Mes transferts à traiter.
5. Valider le transfert comme décrit précedemment.

Pour mémoire, un versement qui serait à la fois non conforme au profil et non conforme au SEDA 0.2 ne peut pas être accepté dans as@lae.

Des listes de formats de fichiers sont associées aux différentes version du SEDA et disponibles sous la forme de `fichier xsd <https://redirect.francearchives.fr/seda/schemas.html>`_

Le tableau suivant récapitule les règles définies :

.. image:: ../img/formats_seda02.png
   :width: 800px
   :height: 74px
   :scale: 100%
   :alt: formats seda
   :align: center

Traiter une demande de communication
====================================

La demande de communication apparaît dans "Communications".
1. Aller dans "validation des demandes de communication"
2. Cliquer sur l'engrenage à droite
3. Cliquer sur accepter

.. image:: ../img/demande_comm_asalae.png
   :width: 900px
   :height: 100px
   :scale: 80%
   :alt: demande de comm
   :align: center

Traiter une demande de restitution
==================================

Une demande de restitution apparaît dans "Restitutions".

Les actions à effectuer pour accepter la restitution sont similaires à celles du transfert.


Traiter une demande d’élimination
=================================

Une demande d’élimination apparaît dans "Eliminations". Elle peut être effectuée pour les archives versées dont la DUA est expirée.

Les actions à effectuer pour accepter la restitution sont similaires à celles du transfert.

Pour qu’une demande d’élimination puisse être traitée dans Asalae, l’outil de conversion de document Cloudoo doit être correctement paramétré.



*-*-*
FIN
*-*-*
Bienvenue sur la documentation du SAEM girondin
===================================

Contents:

Bienvenue sur la documentation de l'application d'archivage électronique SAEM !

Celle-ci comprend 2 parties :

* un guide utilisateur
* un guide d'installation

Le guide utilisateur détaille le fonctionnement des 3 modules qui composent la solution logicielle.

Ces documents sont organisés au sein de différents guides qui s'adressent à différents publics

.. note::

Cette documentation est maintenue par l'équipe projet SAEM.

Les modules SAEM et la documentation sont gratuites et open source et les contributions sont les bienvenues.

Pour contribuer à cette documentation, rendez-vous sur :ref:`/contribution.md`.

Pour contribuer au développement, vous trouverez l'ensemble des sources du projet sur la forge `framagit <http://https://framagit.org/saemproject/>`_

.. toctree::
    :maxdepth: 7
    :numbered:
    :titlesonly:
    :glob:
    :hidden:

  guide-utilisateur/index
  guide-installation/index
  guide-montagne/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

